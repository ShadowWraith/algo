package shadow.algo.string.search;

import java.util.List;

public interface IndexOfSearch {

    List<Integer> allIndexesOf(String where, String what);
}
