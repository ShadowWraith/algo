package shadow.algo.string.search;

import java.util.ArrayList;
import java.util.List;

public class KmpSearch implements IndexOfSearch {

    @Override
    public List<Integer> allIndexesOf(String str, String subStr) {
        List<Integer> entryIndexes = new ArrayList<>();

        int length = str.length();
        int subStrLength = subStr.length();
        int[] longestPrefixes = getLongestPrefixWhichIsAlsoSuffixForEachIndex(subStr);

        int subStrIndex = 0;
        for (int i = 0; i < length; i++) {
            if (str.charAt(i) == subStr.charAt(subStrIndex)) {
                subStrIndex++;
            }
            if (subStrIndex == subStrLength) {
                entryIndexes.add(i - subStrIndex + 1);
                subStrIndex = longestPrefixes[subStrIndex - 1];
            } else if (subStr.charAt(subStrIndex) != str.charAt(i)) {
                if (subStrIndex != 0) {
                    subStrIndex = longestPrefixes[subStrIndex - 1];
                }
            }
        }
        return entryIndexes;
    }

    /**
     * Example:
     * For “AABAACAABAA” answer is [0, 1, 0, 1, 2, 0, 1, 2, 3, 4, 5]
     */
    private int[] getLongestPrefixWhichIsAlsoSuffixForEachIndex(String subStr) {
        int maxPrefixLength = 0;

        int[] longestPrefixes = new int[subStr.length()];
        longestPrefixes[0] = 0;

        for (int i = 1; i < subStr.length(); i++) {
            if (subStr.charAt(i) == subStr.charAt(maxPrefixLength)) {
                longestPrefixes[i] = ++maxPrefixLength;
            } else {
                if (maxPrefixLength != 0) {
                    maxPrefixLength = longestPrefixes[maxPrefixLength - 1];
                }
            }
        }

        return longestPrefixes;
    }
}
