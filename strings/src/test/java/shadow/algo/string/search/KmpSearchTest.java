package shadow.algo.string.search;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Random;

public class KmpSearchTest {

    private final Random random = new Random();

    @Test
    public void shouldFindSubStrInStr() {
        String str = "aaaa";
        String subStr = "a";

        IndexOfSearch search = new KmpSearch();
        List<Integer> entries = search.allIndexesOf(str, subStr);
        Assert.assertNotNull(entries);
        Assert.assertEquals(entries.size(), 4);
    }

    @Test()
    public void shouldFindInTime() {
        int strLength = random.nextInt(100_000_000);
        int subStrLength = random.nextInt(strLength);

        char[] strChars = createStringOfCharWithLength('a', strLength);
        char[] subStrChars = createStringOfCharWithLength('a', subStrLength);
        String str = new String(strChars);
        String subStr = new String(subStrChars);
        List<Integer> entries = new KmpSearch().allIndexesOf(str, subStr);
        Assert.assertEquals(entries.size(), strLength - subStrLength + 1);
    }

    private static char[] createStringOfCharWithLength(char character, int length) {
        char[] chars = new char[length];
        for (int i = 0; i < length; i++) {
            chars[i] = character;
        }
        return chars;
    }
}
