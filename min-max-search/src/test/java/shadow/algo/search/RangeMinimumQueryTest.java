package shadow.algo.search;

import org.testng.annotations.Test;
import shadow.algo.tool.NumberGenerator;

import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertEquals;

public class RangeMinimumQueryTest {

    private final Random random = new Random();

    @Test(invocationCount = 100)
    public void checkSparseTree() {
        int size = 1_000_000;
        List<Integer> randomIntegers = NumberGenerator.randomIntegers(size);
        SparseTree<Integer> tree = new SparseTree<>(randomIntegers);
        for (int i = 0; i < 100; i += 1) {
            int left = random.nextInt(size);
            int right = left + random.nextInt(size - left + 1);
            Integer min = tree.findMinimum(left, right);

            Integer minForCheck = randomIntegers.get(left);
            for (int j = left + 1; j <= right; j++) {
                if (randomIntegers.get(j) < minForCheck) {
                    minForCheck = randomIntegers.get(j);
                }
            }
            assertEquals(min, minForCheck, "Finding min in (" + left + ", " + right + ")");
        }
    }
}
