package shadow.algo.search;

public interface RangeMinimumQuery<T> {

    T findMinimum(int left, int right);
}
