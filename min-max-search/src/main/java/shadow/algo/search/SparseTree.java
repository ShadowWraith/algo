package shadow.algo.search;

import java.lang.reflect.Array;
import java.util.List;

/**
 * This is an implementation of sparse tree. It's useful when you have a lot of requests to find minimum in a range
 * Elements size: n < 2^31
 * Memory consumption: n * log(n, 2)
 * Finding minimum complexity in a range of l,r in [0, n-1]: O(1)
 * @param <T> any comparable type
 */
public class SparseTree<T extends Comparable<T>> implements RangeMinimumQuery<T> {

    private final int elementsSize;
    private final byte highestPowerOf2ForSize;
    private final T[][] tree;

    public SparseTree(List<T> elements) {
        if (elements == null || elements.isEmpty()) {
            throw new IllegalArgumentException("Collection cannot be null or empty");
        }
        elementsSize = elements.size();
        highestPowerOf2ForSize = getHighestPowerOf2(elementsSize);
        // logBase2 + 1 cause 0-th dimension for initial array and logBase2 is for the highest power of 2 which is less than elementsSize
        tree = (T[][]) Array.newInstance(elements.get(0).getClass(), highestPowerOf2ForSize + 1, elementsSize);

        buildTree(elements);
    }

    @Override
    public T findMinimum(int left, int right) {
        int highestPowerOf2ForRange = getHighestPowerOf2(right - left + 1);
        T leftMin = tree[highestPowerOf2ForRange][left];
        T rightMin = tree[highestPowerOf2ForRange][right - (1 << highestPowerOf2ForRange) + 1];
        return leftMin.compareTo(rightMin) <= 0 ? leftMin : rightMin;
    }

    private void buildTree(List<T> elements) {
        if (elements == null || elements.isEmpty()) {
            return;
        }

        addInitialListInTreeAsZeroLevel(elements);

        int powerOf2 = 1;
        for (byte i = 1; i <= highestPowerOf2ForSize; i++) {
            powerOf2 *= 2;
            calculateTreeLevel(powerOf2, i);
        }
    }

    // 2^0 = 1, so for minimum elements[i, j] where i == j is the element at this index
    private void addInitialListInTreeAsZeroLevel(List<T> elements) {
        for (int i = 0; i < elementsSize; i++) {
            tree[0][i] = elements.get(i);
        }
    }

    private void calculateTreeLevel(int powerOf2, byte logBase2) {
        int previousPowerOf2 = powerOf2 / 2;
        for (int i = 0; i + powerOf2 < elementsSize + 1; i++) {
            T leftMin = tree[logBase2 - 1][i];
            T rightMin = tree[logBase2 - 1][i + previousPowerOf2];
            if (leftMin.compareTo(rightMin) <= 0) {
                tree[logBase2][i] = leftMin;
            } else {
                tree[logBase2][i] = rightMin;
            }
        }
    }

    private byte getHighestPowerOf2(int value) {
        return Double.valueOf(Math.log(value) / Math.log(2)).byteValue();
    }
}
