package shadow.algo.tool;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NumberGenerator {

    public static List<Integer> randomIntegers(int count) {
        List<Integer> ints = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            ints.add(random.nextInt());
        }
        return ints;
    }
}
