package shadow.algo.tool;

import java.util.Comparator;
import java.util.List;

public class SequenceChecker {

    public static <T> boolean isAscendant(List<T> elements, Comparator<T> comparator) {
        for (int i = 1; i < elements.size() - 1; i++) {
            if (comparator.compare(elements.get(i), elements.get(i - 1)) < 0) {
                return false;
            }
        }
        return true;
    }
}
