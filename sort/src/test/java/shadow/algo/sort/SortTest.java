package shadow.algo.sort;

import org.testng.annotations.Test;
import shadow.algo.tool.NumberGenerator;
import shadow.algo.tool.SequenceChecker;

import java.util.Comparator;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class SortTest {

    @Test(invocationCount = 10)
    public void bubbleTest() {
        List<Integer> randomIntegers = NumberGenerator.randomIntegers(1000);
        BubbleSort<Integer> sorter = new BubbleSort<>(Comparator.comparingInt(Integer::intValue));
        sorter.sort(randomIntegers);
        assertTrue(SequenceChecker.isAscendant(randomIntegers, Comparator.comparingInt(Integer::intValue)));
    }

    @Test(invocationCount = 10)
    public void quickSortTest() {
        List<Integer> randomIntegers = NumberGenerator.randomIntegers(1000000);
        QuickSort<Integer> sorter = new QuickSort<>(Comparator.comparingInt(Integer::intValue));
        sorter.sort(randomIntegers);
        assertTrue(SequenceChecker.isAscendant(randomIntegers, Comparator.comparingInt(Integer::intValue)));
    }
}
