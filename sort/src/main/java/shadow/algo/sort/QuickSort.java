package shadow.algo.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class QuickSort<T> implements Sort<T> {

    private Comparator<T> comparator;

    public QuickSort(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    @Override
    public List<T> sort(List<T> elements) {
        quickSort(elements, 0, elements.size() - 1);
        return elements;
    }

    private void quickSort(List<T> elements, int lowBorder, int highBorder) {
        int leftIndex = lowBorder;
        int rightIndex = highBorder;
        T pivot = elements.get((leftIndex + rightIndex) / 2);

        while (leftIndex <= rightIndex) {


            while (comparator.compare(elements.get(leftIndex), pivot) < 0) {
                leftIndex += 1;
            }

            while (comparator.compare(elements.get(rightIndex), pivot) > 0) {
                rightIndex -= 1;
            }

            if (leftIndex <= rightIndex) {
                Collections.swap(elements, leftIndex, rightIndex);
                leftIndex += 1;
                rightIndex -= 1;
            }
        }

        if (lowBorder < rightIndex) {
            quickSort(elements, lowBorder, rightIndex);
        }
        if (leftIndex < highBorder) {
            quickSort(elements, leftIndex, highBorder);
        }
    }

    @Override
    public Comparator<T> comparator() {
        return comparator;
    }
}
