package shadow.algo.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BubbleSort<T> implements Sort<T> {

    private Comparator<T> comparator;

    public BubbleSort(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    public List<T> sort(List<T> elements) {
        T min;
        int index;
        for (int i = 0; i < elements.size(); i++) {
            min = elements.get(i);
            index = i;
            for (int j = i + 1; j < elements.size(); j++) {
                if (comparator.compare(elements.get(j), min) < 0) {
                    min = elements.get(j);
                    index = j;
                }
            }
            if (i != index) {
                Collections.swap(elements, i, index);
            }
        }
        return elements;
    }

    @Override
    public Comparator<T> comparator() {
        return comparator;
    }
}
