package shadow.algo.sort;

import java.util.Comparator;
import java.util.List;

public interface Sort<T> {

    List<T> sort(List<T> elements);

    Comparator<T> comparator();
}
